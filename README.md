# carbon-device-mgt

<a href='https://opensource.org/licenses/Apache-2.0'><img src='https://img.shields.io/badge/License-Apache%202.0-blue.svg'></a><br/>

[![pipeline status](https://gitlab.com/entgra/carbon-device-mgt/badges/master/pipeline.svg)](https://gitlab.com/entgra/carbon-device-mgt/commits/master)

Entgra CONNECTED DEVICE MANAGEMENT COMPONENTS

Entgra Connected Device Manager (Entgra CDM) is a comprehensive platform that helps solve mobile computing challenges 
enterprises face today when dealing with both corporate owned, personally enabled (COPE) devices and employee owned devices as part of a bring your own device (BYOD) program.

Whether it is device provisioning, device configuration management, policy enforcement, mobile application 
management, device data security, or compliance monitoring, Entgra CDM offers a single enterprise-grade platform to 
develop extensions for IOT related device types.
